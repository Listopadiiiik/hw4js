  async function getData(url){
    let response = await fetch(url)
    let results = await response.json()

    results.forEach(function(result){

     document.querySelector('.root').insertAdjacentHTML('afterbegin', `<div>${result.episodeId}</div><div>${result.openingCrawl}</div><ul>${result.name}</ul>`)

      result.characters.forEach((urls) => {
        let ul = document.querySelector('ul');
        let li = document.createElement('li');
        fetch(urls).then((response2) => response2.json()).then((data2) => li.innerText = data2.name)
        ul.appendChild(li)
      }
      )
    }
    )
  }

  getData('https://ajax.test-danit.com/api/swapi/films')